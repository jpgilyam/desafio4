import express, { Application } from 'express';
//import { registeredRoutes } from './routes';
import { configureDatabase } from '../database/connection';
//import { configureMiddleware } from './middlewares';
import 'dotenv/config';
import PersonaRoutes from './routes/persona.routes';

const app: Application = express();
const port = process.env.PORT || 5000;


app.use(express.json());
app.set('port', port);
//configureMiddleware(app);
configureDatabase();
//registeredRoutes(app);
app.use('/persona', PersonaRoutes);
// app.use(globalHandler);
export default app;
