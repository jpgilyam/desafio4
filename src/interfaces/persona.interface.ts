import { Document } from 'mongoose'

export default interface IPersona extends Document {
  _id: string;
  nombreCompleto: string;
  email: string;
  contrasenia: string;
  telefono: string;
  rol: string;
  createdAt: Date;
  updatedAt: Date;
};