import { Request, Response } from 'express';
import Persona from "../models/persona.model";
import IPersona from "../interfaces/persona.interface";
//const {Request,Response}= require('express');
//index
export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        const { ...data } = req.query;
        let filters = { ...data };
        console.log(data);
        console.log(filters);
        console.log(data.nombreCompleto);
        if (data.nombreCompleto) {
            console.log("hola");
            filters = { ...filters, nombreCompleto: {$regex: data.nombreCompleto, $options: 'i'} }
        }

        let persona = await Persona.find(filters);

        res.json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal');
    }
};

//show
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findById(id);

        if (!persona)
            res.status(404).send(`No se encontró la persona con id: ${id}`);
        else
            res.json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

//create
export const create =async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;

        const persona: IPersona = new Persona({
            nombreCompleto: data.nombreCompleto,
            email: data.email,
            contrasenia: data.contrasenia,
            telefono: data.telefono,
            rol: data.rol,
        });

        await persona.save();

        res.status(200).json(persona);
        
    } catch (error) {
        console.log(error);
        
        res.status(500).send('Algo salió mal.');
    }
    

}
//update
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    console.log(data);
    console.log(id);
    try {
        let persona = await Persona.findById(id);

        if (!persona)
            return res.status(404).send(`No se encontró la persona con id: ${id}`);
        
        if (data.nombreCompleto) persona.nombreCompleto = data.nombreCompleto;
        if (data.email) persona.email = data.email;
        if (data.contrasenia) persona.contrasenia = data.contrasenia;
        if (data.telefono) persona.telefono = data.telefono;
        if (data.rol) persona.rol = data.rol;

        await persona.save();
        
        res.status(200).json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};
//destroy
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findByIdAndDelete(id);
        console.log(persona);
        if (!persona)
            res.status(404).send(`No se encontró la persona con id: ${id}`);
        else
            res.status(200).json(persona);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

