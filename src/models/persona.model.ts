import { model, Schema } from 'mongoose';
import IPersona from '../interfaces/persona.interface';
//const IPersona = require('../interfaces/persona.interface');
const PersonaSchema = new Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El email es obligatorio'],
        unique: [true, 'Este correo ya esta registrado']
    },
    contrasenia: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    telefono: {
        type: String
    },
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio. Valores posibles: admin/cliente'],
        enum: ['admin', 'cliente']
    }
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IPersona>('Persona', PersonaSchema);